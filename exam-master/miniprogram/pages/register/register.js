// pages/register/register.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      id:'',
      zhanghao:'',
      mima:'',
      zhiwei:'',
  },

   // 获取id
   getId(event){
    // console.log('账号',event.detail.value)
    this.setData({
      id:event.detail.value
    })
  },
    // 获取账号
getZhanghao(event){
  // console.log('账号',event.detail.value)
  this.setData({
    zhanghao:event.detail.value
  })
},
// 获取密码
getMima(event){
  // console.log('密码',event.detail.value)
  this.setData({
    mima:event.detail.value
  })
},
  // 获取职位
  getZhiwei(event){
    // console.log('密码',event.detail.value)
    this.setData({
      zhiwei:event.detail.value
    })
  },
  getType() {
    let that = this;
    wx.request({
      url: 'http://localhost:3000/getType',
      method: 'GET',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success(res) {
        console.log(res.data.data)
      }
    })
  },
  addType() {
    let that = this;
     wx.request({
       url: 'http://localhost:3000/addType',
       method: 'POST',
       data: {
         id:that.data.id,
         zhanghao:that.data.zhanghao,
         mima:that.data.mima,
         zhiwei:that.data.zhiwei
       },
       header: {
         'content-type': 'application/x-www-form-urlencoded'
       },
       success(res) {
         console.log('success')
         wx.navigateTo({
           url: '../login/login',
         })
       }
     })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})