var express = require("express");
var querystring = require('querystring');
var mysql = require('mysql');
// post需要
var bodyParser = require("body-parser");

var app = express();

// post需要
app.use(bodyParser.urlencoded({ extended: true }));

// 跨域
app.all('*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1');
  res.header("Content-Type", "application/json;charset=utf-8");
  next();
});

// 配置数据库信息
var connection = mysql.createConnection({
  host: 'localhost',
  // port: '3306',
  user: 'root',
  password:'wlp991122',
  database: 'myblog'
});

// 连接
connection.connect();

// 获取类型列表
app.get("/getType", function (req, res) {
  var sql = 'select * from users';
  connection.query(sql , function (err, data) {
    if (err) {
      console.log(err);
    } else {
      var result = {
        "status": "200",
        "message": "success",
      }
      result.data = data;
      res.end(JSON.stringify(result));
    }
  });
});

// 添加类型
app.post("/addType", function (req, res) {
  console.log(req.body)
  var params = [req.body.id,req.body.zhanghao,req.body.mima,req.body.zhiwei];
  var sql = "INSERT INTO users(id,zhanghao,mima,zhiwei) VALUES(?,?,?,?)";
  connection.query(sql, params, function (err, data) {
    if (err) {
      res.end('error')
    } else {
      var result = {
        "status": "200",
        "message": 'success',
      }
      res.end(JSON.stringify(result))
    }
  });
});

app.listen(3000);
console.log('3000 running');